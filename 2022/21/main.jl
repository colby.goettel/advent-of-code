using Logging

logger = ConsoleLogger(stderr, Logging.Debug)
global_logger(logger)

"""
Read input file and parse. This will store each line in an array.
"""
function read_input(input_file::String)::Array{String}
  commands::Array{String} = []

  for line ∈ eachline(input_file)
    push!(commands, line)
  end

  return commands
end

"Find the last item in each stack and return a string with each of those letters."
function determine_top_of_stacks(objects::Dict{Int64,Array{Char}})::String
  stack_letters::String = ""

  for stack ∈ sort!(collect(keys(objects)))
    top_letter = last(objects[stack])
    @debug "top letter, stack:" top_letter stack
    stack_letters = string(stack_letters, top_letter)
  end

  return stack_letters
end

"""
Parse move commands from input.

The line format is:

```
move x from y to z
```

Meaning, move (pop from end of array) x objects from stack y to stack z.
"""
function parse_move_commands(line::String, objects::Dict{Int64,Array{Char}})::Nothing
  @debug "move line:" line

  commands = collect(eachmatch(r"\d+", line))

  number_of_moves = parse(Int64, commands[1].match) # Julia is 1-indexed, not 0-indexed
  from_stack = parse(Int64, commands[2].match)
  to_stack = parse(Int64, commands[3].match)
  @debug "move commands:" number_of_moves from_stack to_stack

  for i ∈ 1:number_of_moves
    append!(objects[to_stack], pop!(objects[from_stack]))
  end

  return nothing
end

"Parse objects from input."
function parse_objects(line::String, objects::Dict{Int64,Array{Char}})::Nothing
  @debug "object line:" line
  # Splits input on every three characters with an optional space as the fourth character. Returns an array of RegexMatch objects.
  matches = collect(eachmatch(r"(...) ?", line))

  # Loop through collected objects.
  stack = 1
  for object ∈ matches
    if occursin(r"^ ", object.match)
      @debug "Skipping blank spot"
      stack += 1
      continue
    end

    # Grab substring of object and add to objects dictionary for the appropriate stack. If the stack doesn't already exist, we'll need to create it. Otherwise we can just prepend the object.
    object_substr = object.match[2]
    @debug "object_substr, stack:" object_substr stack
    if haskey(objects, stack)
      pushfirst!(objects[stack], object_substr)
    else
      objects[stack] = [object_substr]
    end
    @debug "objects dict so far:" objects

    stack += 1
  end

  return nothing
end

if abspath(PROGRAM_FILE) == @__FILE__
  @info "Reading input"
  commands = read_input("example-input.txt")
  println(commands)
end
