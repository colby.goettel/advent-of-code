#!/usr/local/bin/julia

# Read in entries
f = open("2.info", "r")

entries = []

for current_line in readlines(f)
  global entries = [entries;current_line]
end

close(f)
# Entries read

# Problem 1: Each line is in the format:
# NUMBER-NUMBER LETTER: PASSWORD
# The NUMBERs give a range for how many times a LETTER must be in the PASSWORD in order to be valid. Return the number of valid passwords in `2.info`.
function problem_1()
  count = 0

  for current_line in entries
    # Interpret line
    # - [1]: lower range: NUMBER from /^/ to /-/
    # - [2]: upper range: NUMBER from /-/ to / /
    # - [3]: LETTER: in position / / to /:/
    # - [4]: PASSWORD: in position /: / to /$/
    items = match(r"^([0-9]+)-([0-9]+) ([a-z]): ([a-z]+)$", current_line)
    lower_range = parse(Int64, items[1])
    upper_range = parse(Int64, items[2])
    letter      = items[3]
    password    = items[4]

    # Count LETTERs in PASSWORD
    matches = length(collect(eachmatch(Regex(letter), password)))
    # Check if the number of matches is within the expected range.
    if lower_range <= matches <= upper_range
      count += 1
    end
  end

  return count
end

# Problem 2: The actual policy is that the LETTER must be present at the LOWER_RANGE xor UPPER_RANGE position, index 1.
function problem_2()
  count = 0

  for current_line in entries
    # Interpret line
    # - [1]: lower range: NUMBER from /^/ to /-/
    # - [2]: upper range: NUMBER from /-/ to / /
    # - [3]: LETTER: in position / / to /:/
    # - [4]: PASSWORD: in position /: / to /$/
    items = match(r"^([0-9]+)-([0-9]+) ([a-z]): ([a-z]+)$", current_line)
    first_position = parse(Int64, items[1])
    last_position  = parse(Int64, items[2])
    letter         = items[3]
    password       = items[4]

    # Check if the letter exists at position A xor B
    A = cmp(letter, string(password[first_position])) == 0
    B = cmp(letter, string(password[last_position])) == 0
    # Easier to read xor function
    if ( A && !B ) || ( !A && B )
      count += 1
    end
  end

  return count
end

println(problem_2())
