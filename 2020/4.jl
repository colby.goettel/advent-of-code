#!/usr/local/bin/julia

# Read in entries
DEBUG = true
if DEBUG
  f = open("4.test", "r")
else
  f = open("4.info", "r")
end

entries = []

for current_line in readlines(f)
  push!(entries, current_line)
end

close(f)
# Entries read

# PROBLEM 1: Each passport entry (separated by a blank line) should contain the following information:
# - byr (Birth Year)
# - iyr (Issue Year)
# - eyr (Expiration Year)
# - hgt (Height)
# - hcl (Hair Color)
# - ecl (Eye Color)
# - pid (Passport ID)
# - cid (Country ID)
# Consider `cid` to be optional. How many valid passports are there?

function count_valid_passports()
  count = 0
  passports = []
  fields = Dict( "byr" => false,
                 "iyr" => false,
                 "eyr" => false,
                 "hgt" => false,
                 "hcl" => false,
                 "ecl" => false,
                 "pid" => false,
                 "cid" => false )

  for current_line in entries
    # On a blank line (meaning a complete entry), add the fields to the passports array.
    if isempty(current_line)
      push!(passports, fields)
      println("fields: $fields")
      for i in passports
        println("dicti: $i")
      end
      println()

      # Reset fields for next run
      replace!(kv -> kv[1] => false, fields)
    end

    # Parse line for each field.
    items = collect(m.captures[1] for m in eachmatch(r"([a-z]{3}):", current_line))

    # Grab the match and mark it as true in the Dict.
    for i in items
      fields["$i"] = true
    end
  end

  # Add the last passport to the passports array.
  println()
  for i in passports
    println(i)
  end
  push!(passports, fields)
  # println(passports)

  # Check if all values in Dict besides `cid` are true. If so, increment valid count.
  for passport in passports
    # println(passport)
    for (k, v) in passport
      # println("$k => $v")
      if v == false && cmp(k, "cid") != 0
        break
      end
    end
  end

  return count
end

println(count_valid_passports())
