#!/usr/local/bin/julia

# Read in entries
f = open("1.info", "r")

amounts = []

for line in readlines(f)
  global amounts = [amounts;parse(Int64,line)]
end

close(f)
# Entries read

# Problem 1: From the numbers in `1.info`, find the two entries that sum to 2020 and print their product.
function part_1()
  for i in amounts
    for j in amounts
      if i + j == 2020
        return i*j
      end
    end
  end
end

# Problem 2: From the numbers in `1.info`, find the three entries that sum to 2020 and print their product.
function part_2()
  for i in amounts
    for j in amounts
      for k in amounts
        if i + j + k == 2020
          return i*j*k
        end
      end
    end
  end
end

println(part_2())
