#!/usr/local/bin/julia

# Read in entries
# f = open("3.test", "r")
f = open("3.info", "r")

entries = []

for current_line in readlines(f)
  global entries = [entries;current_line]
end

close(f)
# Entries read

# PROBLEM 1: Start in the upper left corner. Every hash is a "tree." Move on the slope right 3 down 1. The pattern repeats infinitely to the right. How many trees do you hit?
# Start at the upper left (1,1)
line_number = 1
slope_position = 1

function trees_i_hit(slope_right, slope_down)
  hit_trees = 0

  for i = 1:length(entries)
    if line_number > length(entries)
      break
    end
    current_line = entries[line_number]

    # The map repeats infinitely to the right. This maps our position.
    if slope_position > length(current_line)
      slope_position -= length(current_line)
    end
    # Check if we hit a tree
    if cmp(string(current_line[slope_position]), "#") == 0
      hit_trees += 1
    end

    # Move right three and down one
    global slope_position += slope_right
    global line_number += slope_down
  end

  return hit_trees
end

function product_of(results)
  ret = 1
  for i in results
    ret *= i
  end
  return ret
end

# PROBLEM 2: Now figure it out for each of the following queries and multiply the answers together.
queries = [[1,1],
           [3,1],
           [5,1],
           [7,1],
           [1,2]]

trees = []
count = 1
for i = 1:length(queries)
  global line_number = 1
  global slope_position = 1
  global trees = [trees;trees_i_hit(queries[count][1], queries[count][2])]
  global count += 1
end

result = product_of(trees)

println(result)
