#!/usr/bin/rakudo

# Problem: https://adventofcode.com/2021/day/6

# Each lanternfish creates new lanternfish every 7 days
# The number associated with the fish is how many days until they reproduce:
# - Decrement number.
# - If 0, becomes 6 and creates 1 new fish with attribute 8 (and doesn't decrement until the next day).
# New fish need additional time: two more days for the first cycle.

# Formulaicly, let:
# - a = initial age ($initial_age) - this is the input
# - f = total fish produced including self ($total_fish)
# - d = total days running ($total_days_running) - "after x days"
# Then:
# f = ( ( d - a ) / 5 ) + 1

# Initialize variables
my %lanternfish;

# Test conditions
my $total_days_running = 18;
my $file = '6-test-input.csv';

my $part = 2;
if $part == 1
{
  $total_days_running = 80; # Initial day is always 0
  $file = '6-part1-input.csv';
}
elsif $part == 2
{
  $total_days_running = 10_000_000;
  $file = '6-part1-input.csv';
}

# Open file, read contents into array, and close file handle
my $filehandle = open $file, :r;
%lanternfish{$_}++ for $filehandle.slurp.chop.split(','); # trim trailing whitespace
$filehandle.close;

for 0..$total_days_running-1 -> $day
{
  my %total_fish;

  for %lanternfish.kv -> $days, $count
  {
    if $days == 0
    {
      %total_fish{6} += $count;
      %total_fish{8} += $count;
    }
    else
    {
      %total_fish{$days-1} += $count;
    }
  }

  %lanternfish = %total_fish;
}

# Print total amount of fish
say %lanternfish.values.sum;
